import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


class Creador extends React.Component{

    enviarForm(valor, acciones){

        console.log(valor);

        fetch(
            'http://localhost:4000/insertar',
            {
                method:'POST',
                headers:{'Content-type':'application/json',
                          'cors':'Access-Control-Allow-Origin'},
                body:JSON.stringify({   
                    user:{            
                    id_v:valor.id_v,
                    Tipo_de_evento: valor.Tipo_de_evento,
                    Lugar:valor.Lugar,
                    Fecha_hora:valor.Fecha_hora,
                    Numero_de_personas: valor.Numero_de_personas,
                    Latitud: valor.Latitud,
                    Longitud: valor.Longitud
                    }
                })
            }
        );
    };

    render(){

        let elemento=<Formik 
            
                initialValues={
                    {
                        id_v:'',
                        Tipo_de_evento:'',
                        Lugar:'',
                        Fecha_hora:'',
                        Numero_de_personas:'',
                        Latitud:'',
                        Longitud:''
                    }
                }

            onSubmit={this.enviarForm}

            validationSchema={ Yup.object().shape(
                {
                    id_v: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    Tipo_de_evento: Yup.string().required('Campo es obligatorio'),
                    Lugar: Yup.string().required('Campo es obligatorio'),
                    Fecha_hora: Yup.number().required('Campo es obligatorio'),
                    Numero_de_personas: Yup.number().typeError('Debe ser un número').required('Campo es obligatorio'),
                    Latitud: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    Longitud: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                
                }
            )

            }

            >
                <Container className="p-3">
                    <Form>
                    <h4>FORMULARIO PARA INSCRIBIR EVENTO EN LA LOCALIDAD</h4>
                        <div className="row">
                            <div className="form-group col-md-4">
                            <label htmlFor='id_v'>ID</label>
                            <Field name="id_v" type="text" className="form-control" placeholder="Número unico de evento"/>
                            <ErrorMessage name="id_v" className="invalid-feedback"></ErrorMessage>
                            </div>

                            <div className="form-group col-md-4">
                            <label htmlFor='Tipo_de_evento'>Tipo de evento</label>
                            <Field name="Tipo_de_evento" type="text" className="form-control" />
                            <ErrorMessage name="Tipo_de_evento" className="invalid-feedback"></ErrorMessage>
                            </div>
                        </div>

                        <div className='row'>
                        <div className="form-group col-md-8">
                            <label htmlFor='Lugar'>Lugar del evento</label>
                            <Field name="Lugar" type="text" className="form-control"/>
                            <ErrorMessage name="Lugar" className="invalid-feedback"></ErrorMessage>
                        </div>
                        </div>


                        <div className="row">
                            <div className='form-group col-md-4'>
                            <label htmlFor='Fecha_hora'>Fecha y hora del evento</label>
                            <Field name="Fecha_hora" type="text" className="form-control" placeholder="YYYYMMDDHHMMSS"/>
                            <ErrorMessage name="Fecha_hora" className="invalid-feedback"></ErrorMessage>
                           </div>

                            <div className="form-group col-md-4">
                            <label htmlFor='Numero_de_personas'>Capacidad de personas</label>
                            <Field name="Numero_de_personas" type="text" className="form-control" placeholder="Opcional"/>
                            <ErrorMessage name="Numero_de_personas" className="invalid-feedback"></ErrorMessage>
                            </div>
                        </div>

                        <div className="row">
                            <div className='form-group col-md-4'>
                            <label htmlFor='Latitud'>Latitud del lugar del evento</label>
                            <Field name="Latitud" type="text" className="form-control" placeholder="Tomalo de Google Maps"/>
                            <ErrorMessage name="Latitud" className="invalid-feedback"></ErrorMessage>
                            </div>

                            <div className="form-group col-md-4">
                            <label htmlFor='Longitud'>Longitud del lugar del evento</label>
                            <Field name="Longitud" type="text" className="form-control" placeholder="Tomalo de Google Maps"/>
                            <ErrorMessage name="Longitud" className="invalid-feedback"></ErrorMessage>
                            </div>
                        </div>

                        <div className='row'>
                            <div className="form-group col-md-4">
                                <Button type="submit" className="btn btn-success">Aceptar</Button>
                            </div>
                            <div className="form-group col-md-4">
                                <Button type="reset" className="btn btn-danger">Cancelar</Button>
                            </div>
                        </div>
                    </Form>
                </Container>
            </Formik>;

        return elemento;

    };



}

export default Creador;
