function initMap(){

    var mymap = L.map('visor',  { 
        fullscreenControl : true , 
        fullscreenControlOptions : { 
        position : 'topleft' 
        } 
      }).setView([4.500719, -74.098809],12 );

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        
    }).addTo(mymap);
    
    
    document.getElementById('select-location').addEventListener('change', function(e){
        let coords = e.target.value.split(",");
        L.circle(coords).addTo(mymap);
        mymap.flyTo(coords, 18);
    });

    L.polygon([
        [4.545852, -74.121024],[4.540674, -74.115850],[4.539235, -74.103003],
        [4.546142, -74.103147],[4.539092, -74.098384],[4.538228, -74.094342],
        [4.520097, -74.093764],[4.519521, -74.086547],[4.523982, -74.085392],
        [4.521968, -74.081062],[4.528731, -74.080340],[4.532184, -74.077020],
        [4.543840, -74.074710],[4.541826, -74.070236],[4.534919, -74.071823],
        [4.529019, -74.068648],[4.520673, -74.073411],[4.511607, -74.072978],
        [4.508441, -74.056522],[4.488294, -74.064895],[4.488726, -74.071968],
        [4.469874, -74.076009],[4.469586, -74.085970],[4.469586, -74.085970],
        [4.464117, -74.088135],[4.469586, -74.092032],[4.469442, -74.100405],
        [4.462678, -74.105312],[4.464405, -74.108632],[4.459800, -74.112097],
        [4.468723, -74.119747],[4.468003, -74.122201],[4.463254, -74.126821],
        [4.469010, -74.134327],[4.475342, -74.129707],[4.482970, -74.129274],
        [4.487287, -74.120758],[4.499663, -74.121768],[4.504987, -74.127831],
        [4.520241, -74.125666],[4.524536, -74.129212],[4.527445, -74.123376],
        [4.537371, -74.120629],[4.545756, -74.121144]
    ]).addTo(mymap);
       
    //TURISMO
    var marker1 = L.marker([4.468240, -74.117020]).addTo(mymap);
    marker1.bindPopup("<img src='../Imagenes/diversidad/Granja_Atahualpa2.jpg' class='imagenes1' >" + 
    "<br>GRANJA ATAHUALPA<br>" +  
    "<br> Dirección: <br>Vereda la Requilina.<br>" + 
    "<br> Página web:<br>www.lagranjatahualpa.com<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");

    var marker2 = L.marker([4.478491, -74.093852]).addTo(mymap);
    marker2.bindPopup("<img src='../Imagenes/diversidad/los soches.jpg' class='imagenes1' >" + 
    "<br>AGROPARQUE LOS SOCHES<br>" +  
    "<br> Dirección: <br>Vereda Los Soches, Usme<br>" + 
    "<br> Página web:<br>soches.blogspot.com.co<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");

    var marker3 = L.marker([4.465641, -74.130785]).addTo(mymap);
    marker3.bindPopup("<img src='../Imagenes/diversidad/el refugio.jpg' class='imagenes1' >" + 
    "<br>EL REFUGIO DE SUMAPAZ<br>" +  
    "<br> Dirección:<br>Vereda Chiguasa, Usme<br>" + 
    "<br> Contacto:<br>	311 211 45 79<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");

    var marker4 = L.marker([4.529702, -74.119288]).addTo(mymap);
    marker4.bindPopup("<img src='../Imagenes/diversidad/teatro.jpg' class='imagenes1' >" + 
    "<br>TEATRO PUBLICO MARTINEZ ARDILA<br>" +  
    "<br> Dirección: <br>Portal Usme.<br>" + 
    "<br> Contacto:<br>301 419 0987<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");

    var marker5 = L.marker([4.474551, -74.119637]).addTo(mymap);
    marker5.bindPopup("<img src='https://www.usme.com.co/wp-content/uploads/cache/images/granja-sol-naciente-de-usme/granja-sol-naciente-de-usme-3462497263.jpg' class='imagenes1' >" + 
    "<br>GRANJA SOL NACIENTE<br>" +  
    "<br> Dirección: <br> Carrera 13 # 134 - 10 sur<br>" + 
    "<br> Página web:<br>www.granjasolnaciente.com.co<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");

    var marker6 = L.marker([4.496479, -74.119742]).addTo(mymap);
    marker6.bindPopup("<img src='https://www.usme.com.co/wp-content/uploads/cache/images/parque-cantrarrana-usme-bogota-1/parque-cantrarrana-usme-bogota-1-3573205389.jpg' class='imagenes1' >" + 
    "<br>PARQUE ECOLÓGICO CANTARRANA<br>" +  
    "<br> Dirección: <br>carrera 1A No.100-11 sur<br>" + 
    "<br> Contacto:<br>cantarranamiga@gmail.com<br>" + 
    "<br><a href='../lugares/diversidad.html'>Mas información</a> ");
    
    //DEPORTE
    var marker7 = L.marker([4.468261, -74.117168]).addTo(mymap);
    marker7.bindPopup("<img src='../Imagenes/deporte/mundo-lazo.jpg' class='imagenes1' >" + 
    "<br>MUNDO LAZO X-TREMO<br>" +  
    "<br> Dirección: <br>Vereda La Requilina<br>" + 
    "<br> Página web:<br>www.mundolazoextremo.com<br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");

    var marker8 = L.marker([4.518142, -74.118483]).addTo(mymap);
    marker8.bindPopup("<img src='../Imagenes/deporte/escuela-de-arte.jpg' class='imagenes1' >" + 
    "<br>ESCUELA DE ARTES TALLER SUR<br>" +  
    "<br> Dirección: <br><br>" + 
    "<br> Página web:<br><br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");

    var marker9 = L.marker([4.516942, -74.119018]).addTo(mymap);
    marker9.bindPopup("<img src='../Imagenes/deporte/Club-de-Taekwondo-Shi-Jin-Yu.jpg' class='imagenes1' >" + 
    "<br>CLUB DE TAEKWONDO SHI JIN YU<br>" +  
    "<br> Dirección: <br>Calle 73 B SUR # 1B - 12<br>" + 
    "<br> Contacto:<br>escuelapopulartallersur@gmail.com<br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");

    var marker10 = L.marker([4.498110, -74.119182]).addTo(mymap);
    marker10.bindPopup("<img src='../Imagenes/deporte/clan-cantarra.jpg' class='imagenes1' >" + 
    "<br>CLAN CANTARRANA<br>" +  
    "<br> Dirección: <br>Cra. 1A Bis No. 100-45 Sur<br>" + 
    "<br> Contacto:<br>	3795750 Ext. 3988 y 3989<br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");

    var marker11 = L.marker([4.525990, -74.119072]).addTo(mymap);
    marker11.bindPopup("<img src='../Imagenes/deporte/cancha-de-futbol.jpg' class='imagenes1' >" + 
    "<br>CANCHA DE FÚTBOL<br>" +  
    "<br> Dirección: <br>Av. Usme # 68 B 93 sur<br>" + 
    "<br> Contacto:<br>	311 893 67 40<br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");

    var marker12 = L.marker([4.523622, -74.119944]).addTo(mymap);
    marker12.bindPopup("<img src='../Imagenes/deporte/natural-body-jpg.jpg' class='imagenes1' >" + 
    "<br>NATURAL BODY GYM<br>" +  
    "<br> Dirección: <br>Cll 69 F sur # 14 C 94<br>" + 
    "<br> Contacto:<br>6750389<br>" + 
    "<br><a href='../lugares/deporte.html'>Mas información</a> ");
}


